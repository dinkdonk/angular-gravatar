(function() {
'use strict';

angular.module('ngGravatar', ['md5'])

.factory('$gravatar', ['$q', '$md5', function ($q, $md5) {
	function createSvgFallback(text, size) {
		var textHash = $md5.encode(text);
		var bgColor = '#' + textHash.substring(0, 6);
		var textColor = '#ffffff';

		// If background color is to bright, make text black
		var rgb = parseInt(bgColor.substr(1), 16);
		var r = (rgb >> 16) & 0xff;
		var g = (rgb >> 8) & 0xff;
		var b = (rgb >> 0) & 0xff;

		var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // Per ITU-R BT.709

		if (luma > 230) {
			textColor = '#222222';
		}

		// Extrapolate two letters from text
		var explodedText = text.split(' ');

		if (explodedText.length > 1) {
			text = explodedText[0].charAt(0) + explodedText[explodedText.length - 1].charAt(0);
		} else {
			text = text.substr(0, 2);
		}

		/* eslint-disable indent */
		var svg = [
			"<svg xmlns='http://www.w3.org/2000/svg' width='" + size + "px' height='" + size + "px' viewBox='0 0 100 100'>",
				"<rect x='0' y='0' width='100' height='100' style='fill: " + bgColor + ";'/>",
				"<text text-anchor='middle' dominant-baseline='middle' x='50' y='54' style='font-family: \"Open Sans\", sans-serif; font-weight: 300; font-size: 30pt; fill: " + textColor + ";'>",
					text.toUpperCase(),
				'</text>',
			'</svg>'
		].join('');
		/* eslint-enable indent */

		return 'data:image/svg+xml;base64,' + btoa(svg);
	}

	/**
	 * Rating:
	 * g: suitable for display on all websites with any audience type.
	 * pg: may contain rude gestures, provocatively dressed individuals, the lesser swear words, or mild violence.
	 * r: may contain such things as harsh profanity, intense violence, nudity, or hard drug use.
	 * x: may contain hardcore sexual imagery or extremely disturbing violence.
	 */
	function get(email, size, rating, fallback, fallbackText) {
		var deferred = $q.defer();

		if (typeof email === 'undefined') {
			deferred.resolve(createSvgFallback(fallbackText || '�', size));

			return deferred.promise;
		}

		var url = 'https://www.gravatar.com/avatar/' +
		$md5.encode(email) + '.jpg' +
		'?s=' + size +
		'&r=' + rating;

		if (fallbackText || /^(?!http|www).*?\./i.test(fallback)) {
			url += '&d=404';
		} else {
			url += '&d=' + encodeURIComponent(fallback);

			deferred.resolve(url);
		}

		var img = new Image();

		img.addEventListener('load', function (event) {
			deferred.resolve(url);
		});

		img.addEventListener('error', function (event) {
			if (fallbackText) {
				deferred.resolve(createSvgFallback(fallbackText, size));
			} else {
				// If fallback is relative (local)
				deferred.resolve(fallback);
			}
		});

		img.src = url;

		return deferred.promise;
	}

	return {
		get: get
	};
}])

.directive('gravatarEmail', ['$parse', '$gravatar', function ($parse, $gravatar) {
	function link(scope, element, attrs) {
		var email;
		var size;
		var rating;
		var fallback;
		var fallbackText;

		function update() {
			email = $parse(attrs.gravatarEmail)(scope);
			size = attrs.gravatarSize || 80;
			rating = attrs.gravatarRating || 'g';
			fallback = attrs.gravatarFallback || 'mm';
			fallbackText = attrs.gravatarFallbackText || '';

			$gravatar.get(email, size, rating, fallback, fallbackText)
			.then(function (url) {
				attrs.$set('src', url);
			});
		}

		scope.$watch(attrs.gravatarEmail, _.debounce(function (newValue) {
			if (newValue) {
				update();
			}
		}, 100));

		if (attrs.hasOwnProperty('gravatarFallbackText')) {
			attrs.$observe('gravatarFallbackText', _.debounce(function (newValue) {
				if (newValue) {
					update();
				}
			}, 100));
		}
	}

	return {
		link: link
	};
}]);

})(window.angular);