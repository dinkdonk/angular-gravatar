const gulp = require('gulp');
const gutil = require('gulp-util');
const eslint = require('eslint/lib/cli');
const wrap = require('gulp-wrap');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const browserSync = require('browser-sync');

const packageName = 'angular-gravatar';

gulp.task('default', ['lint', 'build']);

gulp.task('lint', (done) => {
	const code = eslint.execute({'_': ['./src/angular-gravatar.js']});

	if (code) {
		// eslint output already written, wrap up with a short message
		done(new gutil.PluginError('lint:js', new Error('ESLint error. See error log above.')));

		return;
	}

	gutil.log('lint:js', gutil.colors.yellow('If you get $rootScope.$on warnings, they should be ignored (https://github.com/Gillespie59/eslint-plugin-angular/issues/231).'));

	done();
});

function build() {
	return gulp.src(['./src/angular-gravatar.js'])
	.pipe(concat(packageName + '.js'))
	.pipe(wrap('(function() {\n\'use strict\';\n\n<%= contents %>\n\n})(window.angular);'))
	.pipe(gulp.dest('./dist/'));
}

function minify(stream) {
	return stream.pipe(uglify({
		compress: {
			pure_funcs: ['console.log']
		}
	}))
	.pipe(rename({extname: '.min.js'}))
	.pipe(gulp.dest('./dist/'));
}

gulp.task('build-dev', () => {
	return build();
});

gulp.task('build', () => {
	return minify(build());
});

gulp.task('serve', () => {
	const port = parseInt(process.argv[process.argv.indexOf('-p') + 1], 10) || 1338;

	browserSync({
		notify: false,
		port: port,
		server: {
			baseDir: 'demo',
			routes: {
				'/bower_components': 'bower_components',
				'/dist': 'dist'
			}
		},
		files: [
			'./dist/angular-gravatar.js',
			'./demo/index.html'
		]
	});

	gulp.watch([
		'./src/angular-gravatar.js'
	], ['build-dev']);
});